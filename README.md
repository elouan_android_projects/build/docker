## List of docker and docker-compose files to build an Android image

These docker images are made to firstly create a local mirror of the official rockpi4 sources then download these sources and finaly build the android image.   
Each container can be run separately to do one of these tasks

#### See all branches for a specific android image

Branches list :

- __master__ : contain a default config set to no particular android version to use as a base 
- __rockpi4-android7__ : contain a config ready to use to build an android7 image for the rockpi4 board

---

### Files detail
#### Scripts
- __build_all__ : a bash script made to quickly build all docker images
- __init_working_env__ : a bash script made to set up your working environment
#### "Container" tagged folders
- __mirror_container__ : contain the Dockerfile and required files to create a mirror of the aosp sources
- __source_container__ : contain the Dockerfile and required files to download the aosp sources
- __build_container__ : contain the Dockerfile and required files to build an android image
- __build_environment_container__ : contain the Dockerfile and scritps to create a ready to work build environment for android
#### docker_compose folder
- contain the docker_compose yml files

---

### Set up the environment
By default, the working environment is set to `$HOME/Workspaces/Aosp/`.     
The sources mirror will be stored in the sub-folder `mirror` and the working files will be stored in the sub-folder `sources`.      
Every logs will be stored in the sub folder `logs`.

To change it you can use the following line inside the git repository :     
`find . -type f | xargs sed -i 's|$HOME/Workspaces/Aosp/|/<your_environment>/|g'`.

> This is mandatory if you want to save the project data in your host environment.

Then use the `init_working_env` script to create all needed folders

---

### Get docker images
#### Build the images
Use the `build_all` script to quickly build all docker images using the Dockerfiles or build them with the name and tags you choose 
>The `build_all` script require an other custom script named _docker_build_ wich can be found here : https://gitlab.com/elouan/init_linux

---

### Start the android image build
Go to `/docker_compose` and type `docker-compose up` or `docker-compose-<mirror/source/build/buildenv> up` to only start a specific image
>it can take a lot of time so be patient
